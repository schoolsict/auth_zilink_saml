<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//

/*
 * @package     auth_zilink_saml
 * @author      Ian Tasker <ian.tasker@schoolsict.net>
 * @copyright   2010 onwards SchoolsICT Limited, UK (http://schoolsict.net)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
// This plugin was forked from 

/* @originalauthor Martin Dougiamas
 * @author Erlend Strømsvik - Ny Media AS
 * @author Piers Harding - made quite a number of changes
 * @version 1.0
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth/saml
 * @link https://github.com/piersharding/moodle-auth_saml
 */


$SIMPLESAMLPHP_LIB = '/var/www/simplesamlphp';
$SIMPLESAMLPHP_CONFIG = '/var/www/simplesamlphp/config';
$SIMPLESAMLPHP_SP = 'default-sp';
//$SIMPLESAMLPHP_RETURN_TO = 'http://some.other.target'; // for when you need to override RelayState
$SIMPLESAMLPHP_RETURN_TO = null;
// $SIMPLESAMLPHP_ERROR_URL = 'http://some.other.target'; // for when you need to override login error target
$SIMPLESAMLPHP_ERROR_URL = null;

// change this to something specific if you don't want users to be sent to
// Moodle $CFG->wwwroot when logout is completed
$SIMPLESAMLPHP_LOGOUT_LINK = "";  
