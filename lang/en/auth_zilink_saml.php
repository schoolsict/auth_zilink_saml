<?php
// This file is part of Moodle - http://moodle.org/
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package     auth_zilink_saml
 * @author      Ian Tasker <ian.tasker@schoolsict.net>
 * @copyright   2010 onwards SchoolsICT Limited, UK (http://schoolsict.net)
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
// This plugin was forked from 

/* @originalauthor Martin Dougiamas
 * @author Erlend Strømsvik - Ny Media AS
 * @author Piers Harding - made quite a number of changes
 * @version 1.0
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth/saml
 * @link https://github.com/piersharding/moodle-auth_saml
 */
 
global $CFG;

$string['pluginname']         = 'ZiLink - SAML Authentication';
$string['auth_zilink_samltitle']         = 'ZiLink - SAML Authentication';
$string['auth_zilink_samldescription']   = 'SSO Authentication using SimpleSAML. <br/> Do not forget to edit the configuration file: '.$CFG->dirroot.'/auth/auth_saml/config.php';

$string['auth_zilink_saml_dologout'] = 'Log out from Identity Provider';
$string['auth_zilink_saml_dologout_description'] = 'Check to have the module log out from Identity Provider when user log out from Moodle';

$string['auth_zilink_saml_createusers'] = 'Automatically create users';
$string['auth_zilink_saml_createusers_description'] = 'Check to have the module log automatically create users accounts if none exists';

$string['auth_zilink_saml_duallogin'] = 'Enable Dual login for users';
$string['auth_zilink_saml_duallogin_description'] = 'Enable use of users assigned login auth module and SAML';

$string['auth_zilink_saml_notshowusername'] = 'Do not show username';
$string['auth_zilink_saml_notshowusername_description'] = 'Check to have Moodle not show the username for users logging in by Identity Provider';

$string['notconfigured'] = 'auth/zilink_saml is not configured for use';

$string['errorbadlib'] = 'SimpleSAMLPHP lib directory {$a} is not correct.  Please edit the auth/zilink_saml/config.php file correctly.';
$string['errorbadconfig'] = 'SimpleSAMLPHP config directory {$a} is in correct.  Please edit the auth/zilink_saml/config.php file correctly.';

$string['auth_zilink_saml_username'] = 'SAML username mapping';
$string['auth_zilink_saml_username_description'] = 'SAML attribute that is mapped to Moodle username - this defaults to mail';

$string['auth_zilink_saml_username_suffix'] = 'Custom SAML username suffix';
$string['auth_zilink_saml_username_suffix_description'] = 'Append this text to the SAML username attribute';

$string['auth_zilink_saml_username_remove_suffix'] = 'Remove default SAML username suffix';
$string['auth_zilink_saml_username_remove_suffix_description'] = 'Remove the @domain username suffix from the SAML assertion';

$string['auth_zilink_saml_userfield'] = 'Moodle username mapping';
$string['auth_zilink_saml_userfield_description'] = 'Moodle user field that is mapped to SAML username attribute - this defaults to username, but could be idnumber, or email';

$string['auth_zilink_saml_memberattribute'] = 'Member attribute';
$string['auth_zilink_saml_memberattribute_description'] = 'Optional: Overrides user member attribute, when user belongs to a group. Usually \'member\'';
$string['auth_zilink_saml_attrcreators'] = 'Attribute creators';
$string['auth_zilink_saml_attrcreators_description'] = 'List of groups or contexts whose members are allowed to create attributes. Separate multiple groups with \';\'. Usually something like \'cn=teachers,ou=staff,o=myorg\'';
$string['auth_zilink_saml_unassigncreators'] = 'Unassign creators';
$string['auth_zilink_saml_unassigncreators_description'] = 'Unassign creators role if unmatch specified condition.';

$string['retriesexceeded'] = 'Maximum number of retries exceeded ({$a}) - there must be a problem with the Identity Service';
$string['invalidconfig'] = 'Invalid configuration config.php for auth/zilink_saml';
$string['pluginauthfailed'] = 'The SAML authentication plugin failed - user {$a} disallowed (no user auto creation?) or dual login disabled';
$string['pluginauthfailedusername'] = 'The SAML authentication plugin failed - user {$a} disallowed due to invalid username format';
$string['auth_zilink_saml_username_error'] = 'IdP returned a set of data that does not contain the SAML username mapping field. This field is required to login';
$string['loginfailed'] = 'SAML 2.0 login failed when negotiating with the IdP';
